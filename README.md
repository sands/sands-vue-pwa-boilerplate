## SANDS VUE PWA BOILERPLATE

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

<!-- LINK REFERENSI -->
<!-- https://dev.to/pegahsafaie/pwa-for-vue-applications-a-practical-guide-4de3 -->
