// import config from 'config';
import * as AUTHORIZATION from '../network/administrator.apiclient';

export const authorizationService = {
    getAuthorizationList,
    getAuthorizationAll,
    attachPermission,
    detachPermission
}

function getAuthorizationList(token , payload) {
    let API = AUTHORIZATION.GET_AUTHORIZATION_LIST(token, payload.role_id)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function getAuthorizationAll(token) {
    let API = AUTHORIZATION.GET_AUTHORIZATION_ALL(token)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })
    });
}

function attachPermission(token, payload) {
    let params = new URLSearchParams()
    params.append('role_id', payload.role_id)
    params.append('permission_id', payload.permission_id)

    let API = AUTHORIZATION.ATTACH_PERMISSION(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function detachPermission(token, payload) {
    let params = new URLSearchParams()
    params.append('role_id', payload.role_id)
    params.append('permission_id', payload.permission_id)

    let API = AUTHORIZATION.DETACH_PERMISSION(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}




