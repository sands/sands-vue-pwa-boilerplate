// import config from 'config';
import * as DEPARTMENT from '../network/administrator.apiclient';

export const departmentService = {
    getDepartments,
    createDepartment,
    updateDepartment,
    deleteDepartment
}

function getDepartments(token) {
    let API = DEPARTMENT.GET_DEPARTMENT(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })

}

function createDepartment(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('status', payload.status)

    let API = DEPARTMENT.POST_DEPARTMENT(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateDepartment(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('status', payload.status)

    let API = DEPARTMENT.EDIT_DEPARTMENT(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteDepartment(token, payload) {
    let API = DEPARTMENT.DELETE_DEPARTMENT(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}


