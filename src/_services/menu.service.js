// import config from 'config';
import * as AUTH from '../network/auth.apiclient';

export const menuService = {
    getMenus,
    storeMenus,
    getRoleMenu,
    getMenusByRole,
    toggleMenusRole
}

function getMenus(token) {
    let API = AUTH.GET_MENU(token)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })
    });
}

function getMenusByRole(token, role_id) {
    let API = AUTH.GET_MENU_BY_ROLE(token, role_id)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })
    });
}

function storeMenus(token, payload) {
    let params = new URLSearchParams()
    params.append('menus', payload.menus)

    let API = AUTH.STORE_MENU(token, params)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })

    });
}

function toggleMenusRole(token, payload) {
    let params = new URLSearchParams()
    params.append('id', payload.id)
    params.append('role_id', payload.role_id)
    params.append('is_check', payload.is_check)

    let API = AUTH.TOGGLE_ROLE_MENU(token, params)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })

    });
}

function getRoleMenu(token, payload) {
    let id_menu = payload.id
    let API = AUTH.GET_ROLE_MENU(token, id_menu)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })

    });
}







