// import config from 'config';
import * as MODULE from '../network/administrator.apiclient';

export const moduleService = {
    getModules,
    createModule,
    updateModule,
    deleteModule
}

function getModules(token) {
    let API = MODULE.GET_MODULE(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })

}

function createModule(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('display_name', payload.display_name)
    params.append('description', payload.description)
    params.append('status', payload.status)
    params.append('permissions', payload.permissions)
  
    let API = MODULE.POST_MODULE(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateModule(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('display_name', payload.display_name)
    params.append('description', payload.description)
    params.append('status', payload.status)
    params.append('permissions', payload.permissions)

    let API = MODULE.EDIT_MODULE(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteModule(token, payload) {
    let API = MODULE.DELETE_MODULE(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}


