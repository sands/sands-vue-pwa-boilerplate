// import config from 'config';
import * as OCCUPATION from '../network/administrator.apiclient';

export const occupationService = {
    getOccupation,
    createOccupation,
    updateOccupation,
    deleteOccupation
}

function getOccupation(token) {
    let API = OCCUPATION.GET_OCCUPATION(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })

}

function createOccupation(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('status', payload.status)
    params.append('description', payload.description)

    let API = OCCUPATION.POST_OCCUPATION(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateOccupation(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('status', payload.status)
    params.append('description', payload.description)

    let API = OCCUPATION.EDIT_OCCUPATION(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteOccupation(token, payload) {
    let API = OCCUPATION.DELETE_OCCUPATION(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}


