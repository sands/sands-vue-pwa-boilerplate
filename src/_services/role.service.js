// import config from 'config';
import * as ROLE from '../network/administrator.apiclient';

export const roleService = {
    getRoles,
    createRole,
    updateRole,
    deleteRole
}

function getRoles(token) {
    let API = ROLE.GET_ROLE_USER(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })

}

function createRole(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('display_name', payload.display_name)
    params.append('description', payload.description)

    let API = ROLE.POST_ROLE_USER(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateRole(token, payload) {
    let params = new URLSearchParams()
    params.append('name', payload.name)
    params.append('display_name', payload.display_name)
    params.append('description', payload.description)

    let API = ROLE.EDIT_ROLE_USER(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteRole(token, payload) {
    let API = ROLE.DELETE_ROLE_USER(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}


