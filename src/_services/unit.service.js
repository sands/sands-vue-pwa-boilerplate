// import config from 'config';
import * as UNIT from '../network/administrator.apiclient';

export const unitService = {
    getUnits,
    createUnit,
    updateUnit,
    deleteUnit
}

function getUnits(token) {
    let API = UNIT.GET_UNIT_USER(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })

}

function createUnit(token, payload) {
    let params = new URLSearchParams()
    params.append('code_num', payload.code_num)
    params.append('code_str', payload.code_str)
    params.append('desc', payload.desc)

    let API = UNIT.POST_UNIT_USER(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateUnit(token, payload) {
    let params = new URLSearchParams()
    params.append('code_num', payload.code_num)
    params.append('code_str', payload.code_str)
    params.append('desc', payload.desc)

    let API = UNIT.EDIT_UNIT_USER(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteUnit(token, payload) {
    let API = UNIT.DELETE_UNIT_USER(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}


