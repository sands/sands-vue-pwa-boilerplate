// import config from 'config';
import * as ADMIN from '../network/administrator.apiclient';

export const unitUserService = {
    getUsersAccessTable,
    updateUnitUserAccess
}

function getUsersAccessTable(token, payload) {
    let API = ADMIN.GET_UNIT_USER_ACCESS_TABLEDATA(token, payload.page, payload.search, payload.limit)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })
    });

}

function updateUnitUserAccess(token, payload) {
    let params = new URLSearchParams()
    params.append('is_check', payload.is_check)
    params.append('unit_id', payload.unit_id)
    params.append('user_id', payload.user_id)

    let API = ADMIN.UPDATE_UNIT_USER_ACCESS(token, params)

    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })
    });

}
