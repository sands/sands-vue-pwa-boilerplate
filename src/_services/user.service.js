// import config from 'config';
import { authHeader } from '../_helpers'
import {AUTHENTICATE, AUTH_USER} from '../network/auth.apiclient';
import * as ADMIN from '../network/administrator.apiclient';

export const userService = {
    login,
    logout,
    authUser,
    getUsers,
    getUsersTable,
    createUser,
    updateUser,
    deleteUser,
    updateProfile,
    getUsersOccupOperator,
    getUsersOccupChiefCrew
}

function login(email, password) {
    let params = new URLSearchParams()
    params.append('email', email)
    params.append('password', password)

    let API = AUTHENTICATE(params)

    return API.then(response => {
        window.localStorage.removeItem('token')
        window.localStorage.setItem('token', response.data.token)
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function logout() {
    // remove user from local storage to log user out
    window.localStorage.removeItem('user')
    window.localStorage.removeItem('token')
    window.localStorage.removeItem('menus')
}

function authUser(token) {
    let API = AUTH_USER(token)
    return API.then(response => {
        window.localStorage.removeItem('user')
        window.localStorage.setItem('user', JSON.stringify(response.data.data))

        return response.data.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function getUsers(token) {
    let API = ADMIN.GET_USER(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function getUsersOccupOperator(token) {
    let API = ADMIN.GET_USER_OCCUP_OPERATOR(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function getUsersOccupChiefCrew(token) {
    let API = ADMIN.GET_USER_OCCUP_CHIEF_CREW(token)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function getUsersTable(token, payload) {
    let API = ADMIN.GET_USER_TABLEDATA(token, payload.page, payload.search, payload.limit)
    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function createUser(token, payload) {
    let params = new URLSearchParams()

    params.append('name', payload.name)
    params.append('username', payload.username)
    params.append('email', payload.email)
    params.append('status', payload.status)
    params.append('role', payload.role)
    params.append('unit_id', payload.unit_id)
    params.append('occupation_id', payload.occupation_id)
    params.append('department_id', payload.department_id)

    let API = ADMIN.POST_USER(token, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateUser(token, payload) {
    let params = new URLSearchParams()

    params.append('name', payload.name)
    params.append('username', payload.username)
    params.append('email', payload.email)
    params.append('status', payload.status)
    params.append('role', payload.role)
    params.append('unit_id', payload.unit_id)
    params.append('occupation_id', payload.occupation_id)
    params.append('department_id', payload.department_id)
    params.append('is_received_mail', payload.is_received_mail)

    let API = ADMIN.EDIT_USER(token, payload.id, params)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function deleteUser(token, payload) {
    let API = ADMIN.DELETE_USER(token, payload.id)

    return API.then(response => {
        return response.data
    }).catch(e => {
        //ERROR HANDLING
        return Promise.reject(e)
    })
}

function updateProfile(token, payload) {
    let params = new URLSearchParams()

    params.append('fullname', payload.fullname)
    params.append('new_password', payload.new_password)
    params.append('old_password', payload.old_password)

    let API = ADMIN.EDIT_PROFILE(token, payload.id, params)
    return new Promise((resolve, reject) => {
        API.then(response => {
            resolve(response.data);
        }).catch(e => {
            reject(e);
        })

    });
}


