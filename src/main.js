import Vue from 'vue'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import Breadcrumbs from './components/bread_crumbs'
import VueFeather from 'vue-feather'
import Toasted from 'vue-toasted'
import PxCard  from './components/Pxcard.vue'
import Vue2Filters from 'vue2-filters'
import VueSweetalert2 from 'vue-sweetalert2'
import VueFormWizard from 'vue-form-wizard'
import Notifications from 'vue-notification'
import VueApexCharts from 'vue-apexcharts'
import SmartTable from 'vuejs-smart-table'
import VueTimepicker from 'vue2-timepicker'
import ReadMore from 'vue-read-more';

import {authmixin} from "./mixins/auth.mixin"
import {globalmixin} from "./mixins/global.mixin"

import { store } from './store'

Vue.component(PxCard.name, PxCard)

// Import Theme scss
import './assets/scss/app.scss'

import './registerServiceWorker'

import 'vue2-timepicker/dist/VueTimepicker.css'

Vue.use(VueFeather)
Vue.use(Toasted,{
  iconPack: 'fontawesome'
})
Vue.use(BootstrapVue)
Vue.use(SmartTable)
Vue.use(Vue2Filters)
Vue.use(VueFormWizard)
Vue.use(VueSweetalert2)
Vue.use(Notifications)
Vue.use(ReadMore);

Vue.component('apexchart', VueApexCharts)
Vue.component('Breadcrumbs', Breadcrumbs)
Vue.component('VueTimepicker', VueTimepicker)


Vue.mixin(authmixin)
Vue.mixin(globalmixin)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')