export const authmixin = {
  data() {
    return {
      loading: false,
    }
  },
  methods: {
    showAlertSuccess(msg) {
      this.$swal({
        title: "Info!",
        text: msg,
        type: 'success'
      });
    },
    
    showAlertError(msg) {
      this.$swal({
        title: "Error!",
        text: msg,
        type: 'error'
      });
    },

    showToast(msg, type) {
      this.$toasted.show(msg, {
        theme: "outline",
        position: "top-right",
        type: type,
        duration: 3000,
      })
    },

    getMessageAPIError(e){
      this.handleAuthError(e)
      let msg = ""

      if (typeof e.response.data.msg !== 'undefined') {
        msg = e.response.data.msg
      } else {
        msg = (typeof e.response.data.error !== 'undefined') ? e.response.data.error : e.response.data.message
      }
      return msg
    },

    handleAuthError(e) {
      console.log(e)
      if (e.response.status === 401) {
        window.location.href = '/auth/login'
      } 
    },
  },
}
