export const globalmixin = {
  data() {
    return {
    }
  },
  methods: {
    formatRupiah(bilangan, prefix = "Rp") {
      var reverse = bilangan.toString().split('').reverse().join('')
      var ribuan = reverse.match(/\d{1,3}/g)

      ribuan = ribuan.join('.').split('').reverse().join('')

      let rupiah = prefix + " " + ribuan
      return rupiah
    },

    formatDateV1(oldDate) { //16 October 2022
      var date = new Date(oldDate);
      var year = date.getFullYear();
      var month = date.getMonth();
      var day = date.getDate();

      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      var formattedDate = day + '-' + monthNames[month] + '-' + year
      return formattedDate
    },

    formatDatetimeV1(oldDate) { //16 October 2022 15:10:10
      var date = new Date(oldDate);
      var year = date.getFullYear();
      var month = date.getMonth();
      var day = date.getDate();

      var hour = date.getHours()
      var minute = date.getMinutes()
      var sec = date.getSeconds()

      const monthNames = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
      ];
      var formattedDate = day + '-' + monthNames[month] + '-' + year + ` ${hour}:${minute}:${sec}`
      return formattedDate
    },

    arrayRemove(arr, value) {
      return arr.filter(function (ele) {
        return ele != value;
      });
    },

    // aao := is the "associative array" you need to "sort"
    // comp := is the "field" you want to compare or "" if you have no "fields" and simply need to compare values
    // intVal := must be false if you need comparing non-integer values
    // desc := set to true will sort keys in descendant order (default sort order is ascendant)
    sortedKeys(aao, comp = "", intVal = false, desc = false) {
      let keys = Object.keys(aao);

      if (comp != "") {
        if (intVal) {
          if (desc) return keys.sort(function (a, b) { return aao[b][comp] - aao[a][comp] });
          else return keys.sort(function (a, b) { return aao[a][comp] - aao[a][comp] });
        } else {
          if (desc) return keys.sort(function (a, b) {
            return aao[b][comp] < aao[a][comp] ? 1 : aao[b][comp] > aao[a][comp] ? -1 : 0
          });
          else return keys.sort(function (a, b) { return aao[a][comp] < aao[b][comp] ? 1 : aao[a][comp] > aao[b][comp] ? -1 : 0 });
        }
      } else {
        if (intVal) {
          if (desc) return keys.sort(function (a, b) { return aao[b] - aao[a] });
          else return keys.sort(function (a, b) { return aao[a] - aao[b] });
        } else {
          if (desc) return keys.sort(function (a, b) { return aao[b] < aao[a] ? 1 : aao[b] > aao[a] ? -1 : 0 });
          else return keys.sort(function (a, b) { return aao[a] < aao[b] ? 1 : aao[a] > aao[b] ? -1 : 0 });
        }
      }
    }

  },
}
