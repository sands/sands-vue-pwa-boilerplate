import * as NETWORK from './apiclient'

// START USER
export const GET_USER = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/user')
}

export const GET_USER_OCCUP_OPERATOR = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/user/occupation/operator')
}

export const GET_USER_OCCUP_CHIEF_CREW = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/user/occupation/chief-crew')
}

export const GET_USER_TABLEDATA = function (token, page, search, limit) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/user/gettabledata/'+ limit +'?page=' + page + '&search=' + search)
}

export const POST_USER = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/user', params)
}

export const EDIT_USER = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/user/'+ id +'?_method=PUT', params)
}

export const DELETE_USER = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/user/'+ id)
}

export const EDIT_PROFILE = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/user/profile/'+ id +'?_method=PUT', params)
}
// END USER

//START USER UNIT ACCESS
export const GET_UNIT_USER_ACCESS_TABLEDATA = function (token, page, search, limit) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/unit-user-access/gettabledata/'+ limit +'?page=' + page + '&search=' + search)
}

export const UPDATE_UNIT_USER_ACCESS = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/unit-user-access/update-access', params)
}
//END USER UNIT ACCESS


// START AUTHORIZATION
export const GET_AUTHORIZATION_ALL = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/permission')
}

export const GET_AUTHORIZATION_LIST = function (token, id_role) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/permission/authorization-by-role/'+ id_role)
}

export const ATTACH_PERMISSION = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/permission/attachpermission', params)
}

export const DETACH_PERMISSION = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/permission/detachpermission', params)
}
// END AUTHORIZATION


export const GET_ALL_ACTIVITY = function (token, page, search) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/useractivity/gettabledata/10?page=' + page + search)
}

export const GET_AUTH_ACTIVITY = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/authactivity', params)
}

//START UNIT
export const GET_UNIT_USER = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/unit')
}

export const POST_UNIT_USER = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/unit', params)
}

export const EDIT_UNIT_USER = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/unit/'+ id +'?_method=PUT', params)
}

export const DELETE_UNIT_USER = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/unit/'+ id)
}
//END UNIT

//START DEPARTMENT
export const GET_DEPARTMENT = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/department')
}

export const POST_DEPARTMENT = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/department', params)
}

export const EDIT_DEPARTMENT = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/department/'+ id +'?_method=PUT', params)
}

export const DELETE_DEPARTMENT = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/department/'+ id)
}
//END DEPARTMENT

//START MODULE
export const GET_MODULE = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/module')
}

export const POST_MODULE = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/module', params)
}

export const EDIT_MODULE = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/module/'+ id +'?_method=PUT', params)
}

export const DELETE_MODULE = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/module/'+ id)
}
//END MODULE

// START ROLE USER
export const GET_ROLE_USER = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/role')
}

export const POST_ROLE_USER = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/role', params)
}

export const EDIT_ROLE_USER = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/role/'+ id +'?_method=PUT', params)
}

export const DELETE_ROLE_USER = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/role/'+ id)
}
// END ROLE USER

// START OCCUPATION
export const GET_OCCUPATION = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/occupation')
}

export const POST_OCCUPATION = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/occupation', params)
}

export const EDIT_OCCUPATION = function (token, id, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/occupation/'+ id +'?_method=PUT', params)
}

export const DELETE_OCCUPATION = function (token, id) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).delete('/occupation/'+ id)
}
// END OCCUPATION


//GET DATA MASTER PROGRAM GEN21
export const GET_PROGRAM_SLOT_MAINTENANCE = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/get-program-slot-maintenance', params)
}



