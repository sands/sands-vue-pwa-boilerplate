import axios from 'axios';

export var base_url ='http://LOCALPATHAPI/api' //local 
// export var base_url ='http://adr.mncgroup.com:8080/api' //prod 
// export var base_url ='http://adrdev.mncgroup.com:8080/api' //dev 

export const API_CLIENT = axios.create({
  baseURL: base_url,
  headers : {
    'Content-type' : 'application/x-www-form-urlencoded',
  }
})

export const API_CLIENT_WITH_TOKEN = function (token) {
    return axios.create({
        baseURL: base_url,
        headers : {
            'Content-type' : 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            Authorization: "Bearer " + token
        }
    })
}

