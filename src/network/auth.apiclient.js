import * as NETWORK from './apiclient'

export const AUTHENTICATE = function (params) {
  return NETWORK.API_CLIENT.post('/authenticate', params)
}

export const AUTH_USER = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/auth-user')
}

export const GET_MENU = function (token) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/menu')
}

export const STORE_MENU = function (token, params) {
  return NETWORK.API_CLIENT_WITH_TOKEN(token).post('/menu', params)
}


// export const GET_MENU_BY_NAME = function (token, name) {
//   return NETWORK.API_CLIENT_WITH_TOKEN(token).get('/menu/'+name)
// }
