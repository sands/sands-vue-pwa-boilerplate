import Body from '../components/body'
import User from '../pages/admin/user'
import Role from '../pages/admin/role'
import Unit from '../pages/admin/unit'
import Department from '../pages/admin/department'
import Menus from '../pages/admin/menus'
import Module from '../pages/admin/module'
import Authorization from '../pages/admin/authorization'
import Occupation from '../pages/admin/occupation'
import UnitUserAccess from '../pages/admin/unit_user_access'

export const administrator_routes = [
  {
    path: '/administrator',
    component: Body,
    children: [
      {
        path: 'user',
        name: 'administrator.user',
        component: User,
        meta: {
          title: 'Administrator - User',
        }
      },
      {
        path: 'role',
        name: 'administrator.role',
        component: Role,
        meta: {
          title: 'Administrator - Role',
        }
      },
      {
        path: 'module',
        name: 'administrator.module',
        component: Module,
        meta: {
          title: 'Administrator - Module',
        }
      },
      {
        path: 'authorization',
        name: 'administrator.authorization',
        component: Authorization,
        meta: {
          title: 'Administrator - Authorization',
        }
      },
      {
        path: 'unit',
        name: 'administrator.unit',
        component: Unit,
        meta: {
          title: 'Administrator - Unit',
        }
      },
      {
        path: 'department',
        name: 'administrator.department',
        component: Department,
        meta: {
          title: 'Administrator - Department',
        }
      },
      {
        path: 'menu',
        name: 'administrator.menu',
        component: Menus,
        meta: {
          title: 'Administrator - Menus',
        }
      },
      {
        path: 'occupation',
        name: 'administrator.occupation',
        component: Occupation,
        meta: {
          title: 'Administrator - Occupation',
        }
      },
      {
        path: 'unit-user-access',
        name: 'administrator.unit-user-access',
        component: UnitUserAccess,
        meta: {
          title: 'Administrator - Unit User Access',
        }
      },
    ]
  },

]
