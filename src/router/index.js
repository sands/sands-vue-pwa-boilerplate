import Vue from 'vue'
import Router from "vue-router"
import {store} from '../store'

import {administrator_routes} from "./administrator.routes"

import Body from '../components/body'
import Auth from '../components/auth'

/* Auth */
import Login from '../pages/auth/login'
import UserProfile from '../pages/auth/profile'
import Dashboard from '../pages/dashboard'

/* Error Pages */
import Error403 from '../pages/errors/error_403'
import Error404 from '../pages/errors/error_404'

Vue.use(Router)

const routes = [
  { path: '', redirect: { name: 'default' }},
  
  {
    path: '/auth',
    component: Auth,
    children: [
    {
      path: 'login',
      name: 'login',
      component: Login,
      meta: {
        title: ' Login | ADR',
      }
    }
    ]
  },

  {
    path: '/dashboard',
    component: Body,
    children: [
    {
      path: '',
      name: 'default',
      component: Dashboard,
      meta: {
        title: 'Dashboard',
      }
    },
    ]
  },

  {
    path: '/user-profile',
    component: Body,
    children: [
    {
      path: '',
      name: 'user-profile',
      component: UserProfile,
      meta: {
        title: 'User Profile',
      }
    },
    ]
  },



  ...administrator_routes,
];

const router = new Router({
  routes,
  base: '/',
  // base: process.env.BASE_URL,
  mode: 'hash',
  // mode: 'history',
  linkActiveClass: "active",
  scrollBehavior () {
    return { x: 0, y: 0 }
  }
})

router.beforeEach((to, from, next) => {
  let isLoggedIn = store.getters["authentication/getUserIsLoggedIn"]
  if ( to.name !== 'login' && !isLoggedIn ){
    next("/auth/login");
  } else {
    next()
  }
})

export default router
