import Vue from "vue";
import Vuex from "vuex";
// import 'es6-promise/auto';
import layout from './modules/layout'
import menu from './modules/menu'
import { authentication } from './modules/authentication'
import { role } from './modules/role'
import { user } from './modules/user'
import { unit } from './modules/unit'
import { module } from './modules/module'
import { department } from './modules/department'
import { authorization } from './modules/authorization'
import { occupation } from './modules/occupation'
import { unit_user_access } from './modules/unit_user_access'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
    },
    mutations: {
    },
    actions: {
    },
    modules: {
      layout,
      menu,
      authentication,
      role,
      user,
      unit,
      module,
      department,
      authorization,
      occupation,
      unit_user_access
    }
});

