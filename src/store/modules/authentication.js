import { userService, menuService } from '../../_services';
import router from '../../router/index'

const user = JSON.parse(localStorage.getItem('user'))
let token = localStorage.getItem("token")
let app_env = 'local' // local or development or production
let path_api = ''
switch (app_env) {
    case 'local':
        path_api = "http://LOCALPATHAPI"
        break;
    case 'development':
        path_api = "http://PATHDEV:8080"
        break;
    case 'production':
        path_api = "http://PATHPROD:8080"
        break;
    default:
        path_api = "http://127.0.0.1:9000"
        break;
}

let status = user ? true : false
let userData = user ? user : null

export const authentication = {
    namespaced: true,
    state: {
        status: status,
        user: userData,
        token: token,
        app_env: app_env,
        path_api: path_api,
        is_loading_show: false,
        message: '',
        message_type: ''
    },
    getters: {
        getToken: state => {
            return state.token
        },
        getUser: state => {
            return state.user
        },
        getUserIsLoggedIn: state => {
            return state.status
        },
        getPathApi: state => {
            return state.path_api
        }
    },
    actions: {
        login({ dispatch, commit }, { email, password }) {
            commit('loadingStart')

            userService.login(email, password)
                .then(
                    data => {
                        let token = data.token
                        userService.authUser(token).then(
                            user => {
                                commit('loginSuccess', { user: user, token: token })
                                commit('setMessage', { msg: 'Success', type: 'success' })
                                commit('menu/buildMenus', user, { root: true }) // versi static menu

                                setTimeout(() => {
                                    router.push('/')
                                }, 500)

                                commit('loadingEnd')
                            },
                            error => {
                                commit('loginFailure')
                                commit('loadingEnd')
                                commit('setMessage', { msg: error.response.data.msg, type: 'error' })
                            }
                        )

                    },
                    error => {
                        commit('loginFailure')
                        commit('loadingEnd')
                        commit('setMessage', { msg: error.response.data.msg, type: 'error' })
                    }
                )
        },
        logout({ commit }) {
            userService.logout()
            commit('logout')
            router.push('/auth/login')
        },
        updateProfile({ dispatch, commit }, payload) {
            return new Promise((resolve, reject) => {
                userService.updateProfile(token, payload)
                    .then(
                        resp => {
                            resolve(resp);
                        }
                    ).catch(e => {
                        commit('clearMessage')
                        reject(e);
                    })
            });
        },

    },
    mutations: {
        loginSuccess(state, data) {
            state.status = true
            state.user = data.user
            state.token = data.token
        },
        loginFailure(state) {
            state.status = false
            state.user = null
            state.token = null
        },
        loadingStart(state) {
            state.is_loading_show = true
        },
        loadingEnd(state) {
            state.is_loading_show = false
        },
        logout(state) {
            state.status = false
            state.token = null
            state.user = null
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}