import { authorizationService } from '../../_services';
import { authmixin } from '../../mixins/auth.mixin'

let token = localStorage.getItem("token")

export const authorization = {
    namespaced: true,
    state: {
        authorizations: [],
        authorizations_all: [],
        message: '',
        message_type: ''
    },
    getters: {
        getAuthorization: (state) => {
            return state.authorizations
        },
        getPermissionShow: (state) => {
            let permsAll = state.authorizations_all
            let newPerms = []
            permsAll.forEach(el => {
                let name = el.name
                let splitName = name.split("-");
                let permPrefix = splitName[0]
                if(permPrefix == 'show'){
                    newPerms.push(el)
                }
            });

            return newPerms
        },
    },
    actions: {
        fetchAuthorizationList({ dispatch, commit }, payload) {
            authorizationService.getAuthorizationList(token, payload)
                .then(
                    data => {
                        commit('setAuthorizations', data.data)
                    },
                    e => {
                        commit('clearAuthorizations')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        fetchAuthorizationAll({ dispatch, commit }) {
            return new Promise((resolve, reject) => {
                var token = localStorage.getItem("token")
                authorizationService.getAuthorizationAll(token)
                    .then(
                        resp => {
                            commit('setAuthorizationsAll', resp.data)
                            resolve(resp);
                        }
                    ).catch(e => {
                        commit('clearAuthorizationsAll')
                        reject(e);
                    })
            });
        },
        attachPermission({ dispatch, commit }, payload) {
            authorizationService.attachPermission(token, payload)
                .then(
                    data => {
                        let msg = data.msg
                        commit('setMessage', { msg: msg, type: 'success' })
                    },
                    e => {
                        commit('clearAuthorizations')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        detachPermission({ dispatch, commit }, payload) {
            authorizationService.detachPermission(token, payload)
                .then(
                    data => {
                        let msg = data.msg
                        commit('setMessage', { msg: msg, type: 'success' })
                    },
                    e => {
                        commit('clearAuthorizations')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
    },
    mutations: {
        setAuthorizations(state, authorizations) {
            state.authorizations = authorizations
        },
        clearAuthorizations(state) {
            state.authorizations = []
        },
        setAuthorizationsAll(state, authorizations) {
            state.authorizations_all = authorizations
        },
        clearAuthorizationsAll(state) {
            state.authorizations_all = []
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}