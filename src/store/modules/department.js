import { departmentService } from '../../_services';
import {authmixin} from '../../mixins/auth.mixin'

export const department = {
    namespaced: true,
    state: {
        departments: [],
        message: '',
        message_type: ''
    },
    getters: {
        getDepartments: (state) => {
            var newDept = (state.departments).filter(function(val, index){
                let status = val.status 
                if(status == 1 || status == "1"){
                    return val
                }
            })
            // return state.departments
            return newDept
        },

        getDepartmentsAll: (state) => {
            return state.departments
        },
    },
    actions: {
        fetchDepartments({ dispatch, commit }) {
            let token = localStorage.getItem("token")
            
            departmentService.getDepartments(token)
                .then(
                    data => {
                        commit('setDepartments', data.data)
                    },
                    e => {
                        commit('clearDepartments')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            
            departmentService.createDepartment(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchDepartments")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        update({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            
            departmentService.updateDepartment(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchDepartments")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        delete({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            
            departmentService.deleteDepartment(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchDepartments")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        }
    },
    mutations: {
        setDepartments(state, departments) {
            state.departments = departments
        },
        clearDepartments(state) {
            state.departments = []
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}