import { masterProgramService } from '../../_services';
import router from '../../router/index'

let token = localStorage.getItem("token")

export const master_program = {
    namespaced: true,
    state: {
        programs: [],
    },
    getters: {
        getPrograms: (state) => {
            return state.programs
        },
    },
    actions: {
        fetchProgramMasterGen21({ dispatch, commit }, payload) {
            return new Promise((resolve, reject) => {
                masterProgramService.getProgramMasterGen21(token, payload)
                    .then(
                        resp => {
                            resolve(resp);
                            commit('setPrograms', resp.data)
                        }
                    ).catch(e => {
                        commit('clearPrograms')
                        reject(e);
                    })
            });
        }
    },
    mutations: {
        setPrograms(state, data) {
            state.programs = data
        },
        clearPrograms(state) {
            state.programs = []
        }
    }
}