import MenuSuperadmin from '../../data/menu_superadmin'
import MenuUser from '../../data/menu_user'

import router from '../../router/index'
import { authmixin } from '../../mixins/auth.mixin'
import { userService, menuService } from '../../_services'

let user = localStorage.getItem("user")
let token = localStorage.getItem("token")
let menus = localStorage.getItem("menus")
menus = (!menus) ? [] : JSON.parse(menus)

//list ROLE hardcode
const ROLE_SUPERADMIN = 'super-admin'
const ROLE_ADMIN = 'admin'
const ROLE_USER = 'user'

const state = {
  data: menus,
  searchData: [],
  menus: [],
  buildMenus: [],
  roleMenusByIdMenu: [],
  togglesidebar: true
}

// getters
const getters = {
  getMenus: (state) => {
    return state.menus
  },
  getBuildMenus: (state) => {
    let dataBuildNew = {}
    let buildMenus = state.buildMenus
    for (var key in buildMenus) {
      let parentMenu = buildMenus[key]
      let seq = parentMenu.seq
      let isHaveChild = parentMenu.children == null ? false : true
      if (isHaveChild == true) {
        let childrenMenu = parentMenu.children
        let lastChildSeq = Object.keys(childrenMenu).length

        for (var keyC in childrenMenu) {
          let childMenu = childrenMenu[keyC]
          childMenu['last_seq'] = lastChildSeq
        }
      }

      dataBuildNew[seq] = parentMenu
      dataBuildNew[seq]['last_seq'] = Object.keys(buildMenus).length
    }

    return dataBuildNew
  },
  getRoleMenusByIdMenu: (state) => {
    return state.roleMenusByIdMenu
  }
}

// mutations
const mutations = {
  opensidebar: (state) => {
    state.togglesidebar = !state.togglesidebar
  },
  buildMenus: (state, userLogin) => {
    var menus = []
    var roleName = ''
console.log(userLogin, "userLogin")
    if (typeof userLogin != "undefined") {
      roleName = userLogin.role_name
      if (roleName == ROLE_SUPERADMIN || roleName == ROLE_ADMIN) {
        menus = MenuSuperadmin.data
      } else if (roleName == ROLE_USER) {
        menus = MenuUser.data
      }

      window.localStorage.setItem('menus', JSON.stringify(menus))
      state.data = menus
    }
  },
  buildMenusDynamic: (state, menusByRole) => {
    var menus = []
    for (var idx in menusByRole) {
      let menu = menusByRole[idx]
      let isParentMenu = typeof menu.children == 'undefined' ? true : false
      if (isParentMenu) {
        menus.push({
          "path": "/" + menu.url,
          "title": menu.display_name,
          "icon": menu.icon,
          "type": menu.type,
          "active": false
        })
      } else {
        let childrenMenu = menu.children
        let childrenMenuNew = []
        for (var idx in childrenMenu) {
          let child = childrenMenu[idx]
          childrenMenuNew.push({
            "path": "/" + child.url,
            "title": child.display_name,
            "type": child.type
          })
        }
        menus.push({
          "path": "/" + menu.url,
          "title": menu.display_name,
          "icon": menu.icon,
          "type": menu.type,
          "active": false,
          "badgeType": "primary",
          "children": childrenMenuNew
        })

      }
    }

    window.localStorage.setItem('menus', JSON.stringify(menus))
    state.data = menus
  },
  resizetoggle: (state) => {
    if (window.innerWidth < 1199) {
      state.togglesidebar = false
    } else {
      state.togglesidebar = true
    }
  },
  searchTerm: (state, term) => {
    let items = [];
    var searchval = term.toLowerCase();
    state.data.filter(menuItems => {
      if (menuItems.title) {
        if (menuItems.title.toLowerCase().includes(searchval) && menuItems.type === 'link') {
          items.push(menuItems);
        }
        if (!menuItems.children) return false
        menuItems.children.filter(subItems => {
          if (subItems.title.toLowerCase().includes(searchval) && subItems.type === 'link') {
            subItems.icon = menuItems.icon
            items.push(subItems);
          }
          if (!subItems.children) return false
          subItems.children.filter(suSubItems => {
            if (suSubItems.title.toLowerCase().includes(searchval)) {
              suSubItems.icon = menuItems.icon
              items.push(suSubItems);
            }
          })
        })
        state.searchData = items
      }
    });
  },
  setNavActive: (state, item) => {
    if (!item.active) {
      state.data.forEach(a => {
        if (state.data.includes(item))
          a.active = false
        if (!a.children) return false
        a.children.forEach(b => {
          if (a.children.includes(item)) {
            b.active = false
          }
        })
      });
    }
    item.active = !item.active
  },
  setActiveRoute: (state, item) => {
    state.data.filter(menuItem => {
      if (menuItem != item)
        menuItem.active = false
      if (menuItem.children && menuItem.children.includes(item))
        menuItem.active = true
      if (menuItem.children) {
        menuItem.children.filter(submenuItems => {
          if (submenuItems.children && submenuItems.children.includes(item)) {
            menuItem.active = true
            submenuItems.active = true
          }
        })
      }
    })
  },
  setMenus(state, data) {
    state.menus = data
  },
  setBuildMenus(state, data) {
    state.buildMenus = data
  },
  clearMenus(state) {
    state.menus = []
  },
  setRoleMenusByIdMenu(state, data) {
    state.roleMenusByIdMenu = data
  }
};

// actions
const actions = {
  opensidebar: (context, term) => {
    context.commit('opensidebar', term)
  },
  resizetoggle: (context, term) => {
    context.commit('resizetoggle', term)
  },
  searchTerm: (context, term) => {
    context.commit('searchTerm', term)
  },
  setNavActive: (context, item) => {
    context.commit('setNavActive', item)
  },
  setActiveRoute: (context, item) => {
    context.commit('setActiveRoute', item)
  },
  saveMenu({ dispatch, commit }, payload) {
    return new Promise((resolve, reject) => {
      var token = localStorage.getItem("token")
      menuService.storeMenus(token, payload)
        .then(
          resp => {
            resolve(resp);
          }
        ).catch(e => {
          reject(e);
        })
    });
  },
  fetchMenus({ dispatch, commit }) {
    return new Promise((resolve, reject) => {
      var token = localStorage.getItem("token")
      menuService.getMenus(token)
        .then(
          resp => {
            commit('setMenus', resp.data)
            commit('setBuildMenus', resp.data)
            resolve(resp);
          }
        ).catch(e => {
          commit('clearMenus')
          reject(e);
        })
    });
  },
  checkRoleMenu({ dispatch, commit }, payload) {
    return new Promise((resolve, reject) => {
      var token = localStorage.getItem("token")
      menuService.toggleMenusRole(token, payload)
        .then(
          resp => {
            resolve(resp);
          }
        ).catch(e => {
          reject(e);
        })
    });
  },
  fetchRoleMenu({ dispatch, commit }, payload) {
    return new Promise((resolve, reject) => {
      var token = localStorage.getItem("token")
      menuService.getRoleMenu(token, payload)
        .then(
          resp => {
            commit('setRoleMenusByIdMenu', resp.data)
            resolve(resp);
          }
        ).catch(e => {
          reject(e);
        })
    });
  },
  moveUpMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_id_menu == null ? true : false
    let idMenu = payload.id_menu
    let seqMenu = payload.seq_menu
    let parentIdMenu = payload.parent_id_menu
    let parentSeq = isParentMenu == false ? payload.parent_seq : null
    let buildMenus = getters.getBuildMenus

    if (isParentMenu) {
      let menuTarget = buildMenus[seqMenu]
      let menuTargetSeq = menuTarget.seq
      let menuSwitchSeq = menuTargetSeq - 1
      let menuSwitch = buildMenus[menuSwitchSeq]

      //switch
      buildMenus[seqMenu] = menuSwitch
      buildMenus[menuSwitchSeq] = menuTarget

      buildMenus[seqMenu].seq = menuTargetSeq
      buildMenus[menuSwitchSeq].seq = menuSwitchSeq
    } else {
      let childrenMenu = buildMenus[parentSeq]['children']
      let menuTarget = childrenMenu[seqMenu]

      let menuTargetSeq = menuTarget.seq
      let menuSwitchSeq = menuTargetSeq - 1
      let menuSwitch = childrenMenu[menuSwitchSeq]

      //switch
      childrenMenu[menuTargetSeq] = menuSwitch
      childrenMenu[menuSwitchSeq] = menuTarget

      childrenMenu[menuTargetSeq].seq = menuTargetSeq
      childrenMenu[menuSwitchSeq].seq = menuSwitchSeq
    }

    commit('setBuildMenus', buildMenus)
  },
  moveDownMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_id_menu == null ? true : false
    let idMenu = payload.id_menu
    let seqMenu = payload.seq_menu
    let parentIdMenu = payload.parent_id_menu
    let parentSeq = isParentMenu == false ? payload.parent_seq : null
    let buildMenus = getters.getBuildMenus

    if (isParentMenu) {
      let menuTarget = buildMenus[seqMenu]
      let menuTargetSeq = menuTarget.seq
      let menuSwitchSeq = menuTargetSeq + 1
      let menuSwitch = buildMenus[menuSwitchSeq]

      //switch
      buildMenus[seqMenu] = menuSwitch
      buildMenus[menuSwitchSeq] = menuTarget

      buildMenus[seqMenu].seq = menuTargetSeq
      buildMenus[menuSwitchSeq].seq = menuSwitchSeq
    } else {
      let childrenMenu = buildMenus[parentSeq]['children']
      let menuTarget = childrenMenu[seqMenu]
      let menuTargetSeq = menuTarget.seq
      let menuSwitchSeq = menuTargetSeq + 1
      let menuSwitch = childrenMenu[menuSwitchSeq]

      //switch
      childrenMenu[menuTargetSeq] = menuSwitch
      childrenMenu[menuSwitchSeq] = menuTarget

      childrenMenu[menuTargetSeq].seq = menuTargetSeq
      childrenMenu[menuSwitchSeq].seq = menuSwitchSeq
    }

    commit('setBuildMenus', buildMenus)
  },
  deleteMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_id_menu == null ? true : false
    let idMenu = payload.id_menu
    let seqMenu = payload.seq_menu
    let parentIdMenu = payload.parent_id_menu
    let parentSeq = isParentMenu == false ? payload.parent_seq : null
    let buildMenus = getters.getBuildMenus
    let buildMenusNew = {}

    if (isParentMenu) {
      delete buildMenus[seqMenu]

      //rebuild 
      let newSeq = 1
      for (var key in buildMenus) {
        buildMenusNew[newSeq] = buildMenus[key]
        buildMenusNew[newSeq].seq = newSeq
        newSeq += 1
      }
    } else {
      let childrenMenu = buildMenus[parentSeq]['children']
      delete childrenMenu[seqMenu]

      let newSeq = 1
      let buildChildrenMenusNew = {}
      for (var key in childrenMenu) {
        buildChildrenMenusNew[newSeq] = childrenMenu[key]
        buildChildrenMenusNew[newSeq].seq = newSeq
        newSeq += 1
      }

      buildMenus[parentSeq]['children'] = buildChildrenMenusNew
      buildMenusNew = buildMenus
    }

    commit('setBuildMenus', buildMenusNew)
  },
  updateMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_seq == null ? true : false
    let displayName = payload.display_name
    let icon = payload.icon
    let name = payload.name
    let permissionId = payload.permission_id
    let url = payload.url
    let seq = payload.seq
    let type = payload.type
    let status_active = payload.status_active
    let parentSeq = payload.parent_seq
    let buildMenus = getters.getBuildMenus
    let buildMenusNew = {}

    if (isParentMenu == true) {
      let menuSelected = buildMenus[seq]
      menuSelected.display_name = displayName
      menuSelected.icon = icon
      menuSelected.name = name
      menuSelected.type = type
      menuSelected.status_active = status_active
      menuSelected.permission_id = permissionId
      menuSelected.url = url
    } else {
      let menuChildSelected = buildMenus[parentSeq].children[seq]
      menuChildSelected.display_name = displayName
      menuChildSelected.icon = icon
      menuChildSelected.name = name
      menuChildSelected.type = type
      menuChildSelected.status_active = status_active
      menuChildSelected.permission_id = permissionId
      menuChildSelected.url = url
    }

    buildMenusNew = buildMenus
    commit('setBuildMenus', buildMenusNew)
  },
  addMenu({ commit, getters }, payload) {
    let buildMenus = getters.getBuildMenus
    let buildMenusLen = Object.keys(buildMenus).length
    let buildMenusNew = {}

    let displayName = payload.display_name
    let icon = payload.icon
    let name = payload.name
    let permissionId = payload.permission_id
    let url = payload.url
    let type = payload.type
    let status_active = payload.status_active
    let seq = buildMenusLen + 1

    let addMenu = {
      display_name: displayName,
      icon: icon,
      id: Math.random().toString(36).slice(2, 9),
      // last_seq: seq,
      name: name,
      permission_id: permissionId,
      seq: seq,
      type: type,
      status_active: status_active,
      url: url
    }

    buildMenus[seq] = addMenu

    buildMenusNew = buildMenus
    commit('setBuildMenus', buildMenusNew)
  },
  mergeUpMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_id_menu == null ? true : false
    let seqMenu = payload.seq_menu
    let parentIdMenu = payload.parent_id_menu
    let parentSeq = isParentMenu == false ? payload.parent_seq : null
    let buildMenus = getters.getBuildMenus
    let buildMenusNew = {}

    if (isParentMenu) {
      let menuTarget = buildMenus[seqMenu]
      let menuTargetSeq = menuTarget.seq
      let menuSwitchSeq = menuTargetSeq - 1
      let menuSwitch = buildMenus[menuSwitchSeq]
      let menuSwitchChildren = buildMenus[menuSwitchSeq]['children']
      let menuSwitchChildrenLen = typeof menuSwitchChildren == 'undefined' ? 0 : Object.keys(menuSwitchChildren).length

      menuTarget.seq = menuSwitchChildrenLen + 1

      let newSeq = menuSwitchChildrenLen + 1
      let buildChildrenMenusNew = typeof menuSwitchChildren == 'undefined' ? {} : menuSwitchChildren
      buildChildrenMenusNew[newSeq] = menuTarget
      buildChildrenMenusNew[newSeq]['parent_id'] = menuSwitch.id
      buildChildrenMenusNew[newSeq]['parent_seq'] = menuSwitch.seq

      menuSwitch['children'] = buildChildrenMenusNew

      buildMenus[menuSwitchSeq]['children'] = buildChildrenMenusNew
      delete buildMenus[menuTargetSeq]

      buildMenusNew = buildMenus
    }

    commit('setBuildMenus', buildMenusNew)
  },
  mergeDownMenu({ commit, getters }, payload) {
    let isParentMenu = payload.parent_id_menu == null ? true : false
    let seqMenu = payload.seq_menu
    let parentIdMenu = payload.parent_id_menu
    let parentSeq = isParentMenu == false ? payload.parent_seq : null
    let buildMenus = getters.getBuildMenus
    let buildMenusNew = buildMenus
    let lengMenu = Object.keys(buildMenusNew).length

    if (!isParentMenu) {
      let childrenMenu = buildMenusNew[parentSeq]['children']
      buildMenusNew[lengMenu + 1] = childrenMenu[seqMenu]

      //delete item in source
      delete childrenMenu[seqMenu]

      //delete field in new menu parent
      delete buildMenusNew[lengMenu + 1]['parent_id']
      delete buildMenusNew[lengMenu + 1]['parent_seq']
      buildMenusNew[lengMenu + 1]['seq'] = lengMenu + 1
    }

    commit('setBuildMenus', buildMenusNew)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}