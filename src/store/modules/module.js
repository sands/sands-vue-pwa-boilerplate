import { moduleService } from '../../_services';
import router from '../../router/index'
import { authmixin } from '../../mixins/auth.mixin'

let token = localStorage.getItem("token")

export const module = {
    namespaced: true,
    state: {
        modules: [],
        message: '',
        message_type: '',
        permissions_prefix: [{
            id: 'show',
            check: false,
        },
        {
            id: 'create',
            check: false,
        },
        {
            id: 'update',
            check: false,
        },
        {
            id: 'delete',
            check: false,
        },
        ],
    },
    getters: {
        getModules: (state) => {
            return state.modules
        },
    },
    actions: {
        fetchModules({ dispatch, commit }) {
            moduleService.getModules(token)
                .then(
                    data => {
                        commit('setModules', data.data)
                    },
                    e => {
                        commit('clearModules')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({ dispatch, commit }, payload) {
            moduleService.createModule(token, payload)
                .then(
                    data => {
                        let msg = data.msg
                        dispatch("fetchModules")
                        commit('setMessage', { msg: msg, type: 'success' })
                    },
                    e => {
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        update({ dispatch, commit }, payload) {
            moduleService.updateModule(token, payload)
                .then(
                    data => {
                        let msg = data.msg
                        dispatch("fetchModules")
                        commit('setMessage', { msg: msg, type: 'success' })
                    },
                    e => {
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        delete({ dispatch, commit }, payload) {
            moduleService.deleteModule(token, payload)
                .then(
                    data => {
                        let msg = data.msg
                        dispatch("fetchModules")
                        commit('setMessage', { msg: msg, type: 'success' })
                    },
                    e => {
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        checkPermissionsOnUpdate({ dispatch, commit }, payload) {
            commit('setPermissionCheckboxOnUpdate', { permissions: payload.permissions })
        },
        checkPermissionsOnChange({ dispatch, commit }, payload) {
            commit('setPermissionCheckboxOnChange', { permission: payload.permission, type: payload.type })
        }
    },
    mutations: {
        setModules(state, modules) {
            state.modules = modules
        },
        setPermissionCheckboxOnUpdate(state, payload) {
            let perms = payload.permissions
            perms.forEach((el) => {
                let name = el.name
                let splitName = name.split("-")[0]
                let permissions_prefix = state.permissions_prefix
                permissions_prefix.forEach((elPrefix) => {
                    if (splitName == elPrefix.id) {
                        elPrefix.check = true
                    }
                });
            });
        },
        setPermissionCheckboxOnChange(state, payload) {
            let permCheck = payload.permission
            let type = payload.type
            let permissions_prefix = state.permissions_prefix

            permissions_prefix.forEach((elPrefix) => {
                if (permCheck == elPrefix.id) {
                    elPrefix.check = (type == 'check') ? true : false
                }
            });
        },
        clearModules(state) {
            state.modules = []
        },
        clearPermissions(state) {
            (state.permissions_prefix).forEach((el) => {
                el.check = false;
            });
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}