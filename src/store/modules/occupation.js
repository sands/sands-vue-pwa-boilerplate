import { occupationService } from '../../_services';
import router from '../../router/index'
import {authmixin} from '../../mixins/auth.mixin'

export const occupation = {
    namespaced: true,
    state: {
        occupations: [],
        message: '',
        message_type: ''
    },
    getters: {
        getOccupation: (state) => {
            return state.occupations
        },
    },
    actions: {
        fetchOccupation({ dispatch, commit }) {
            let token = localStorage.getItem("token")
            occupationService.getOccupation(token)
                .then(
                    data => {
                        commit('setOccupation', data.data)
                    },
                    e => {
                        commit('clearOccupation')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            occupationService.createOccupation(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchOccupation")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        update({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            occupationService.updateOccupation(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchOccupation")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        delete({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")
            occupationService.deleteOccupation(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchOccupation")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        }
    },
    mutations: {
        setOccupation(state, occupations) {
            state.occupations = occupations
        },
        clearOccupation(state) {
            state.occupations = []
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}