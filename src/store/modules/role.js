import { roleService } from '../../_services';
import router from '../../router/index'
import {authmixin} from '../../mixins/auth.mixin'

export const role = {
    namespaced: true,
    state: {
        roles: [],
        message: '',
        message_type: ''
    },
    getters: {
        getRoles: (state) => {
            return state.roles
        },
    },
    actions: {
        fetchRoles({ dispatch, commit }) {
            let token = localStorage.getItem("token")

            roleService.getRoles(token)
                .then(
                    data => {
                        commit('setRoles', data.data)
                    },
                    e => {
                        commit('clearRoles')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            roleService.createRole(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchRoles")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        update({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            roleService.updateRole(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchRoles")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        delete({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            roleService.deleteRole(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchRoles")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        }
    },
    mutations: {
        setRoles(state, roles) {
            state.roles = roles
        },
        clearRoles(state) {
            state.roles = []
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}