import { unitService } from '../../_services';
import {authmixin} from '../../mixins/auth.mixin'

export const unit = {
    namespaced: true,
    state: {
        units: [],
        message: '',
        message_type: ''
    },
    getters: {
        getUnits: (state) => {
            return state.units
        },
    },
    actions: {
        fetchUnits({ dispatch, commit }) {
            let token = localStorage.getItem("token")

            unitService.getUnits(token)
                .then(
                    data => {
                        commit('setUnits', data.data)
                    },
                    e => {
                        commit('clearUnits')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            unitService.createUnit(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchUnits")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        update({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            unitService.updateUnit(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchUnits")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        delete({dispatch, commit}, payload) {
            let token = localStorage.getItem("token")

            unitService.deleteUnit(token, payload)
            .then(
                data => {
                    let msg = data.msg
                    dispatch("fetchUnits")
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        }
    },
    mutations: {
        setUnits(state, units) {
            state.units = units
        },
        clearUnits(state) {
            state.units = []
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        }
    }
}