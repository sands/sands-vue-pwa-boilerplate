import { unitUserService } from '../../_services';
import router from '../../router/index'
import {authmixin} from '../../mixins/auth.mixin'

let token = localStorage.getItem("token")

export const unit_user_access = {
    namespaced: true,
    state: {
        users: [],
        current_page: 1,
        last_page: 1,
        total_rows: 0,
        per_page: 10,
        page_options: [5, 10, 20, 50, {
            value: Number.MAX_SAFE_INTEGER,
            text: "show all"
        }],
        filter: '',
    },
    getters: {
        getUsers: (state) => {
            return state.users
        },
        getPerPage: (state) => {
            return state.per_page
        },
        getTotalRows: (state) => {
            return state.total_rows
        },
    },
    actions: {
        fetchUserUnitAccess({ commit }, payload) {
            return new Promise((resolve, reject) => {
                unitUserService.getUsersAccessTable(token, payload)
                    .then(
                        resp => {
                            commit('setUsers', resp.data)
                            resolve(resp);
                        }
                    ).catch(e => {
                        commit('clearUsers')
                        reject(e);
                    })
            });
        },
        changeUnitUserAccess({ commit }, payload) {
            return new Promise((resolve, reject) => {
                unitUserService.updateUnitUserAccess(token, payload)
                    .then(
                        resp => {
                            resolve(resp);
                        }
                    ).catch(e => {
                        commit('clearUsers')
                        reject(e);
                    })
            });
        },

     
    },
    mutations: {
        setUsers(state, data) {
            state.users = data.data
            state.current_page = data.current_page
            state.last_page = data.last_page
            state.total_rows = data.total
        },
        clearUsers(state) {
            state.users = []
        },
        changeLimitPage(state, data){
            state.per_page = data
        },
        changeFilter(state, data){
            state.filter = data
        }
    }
}