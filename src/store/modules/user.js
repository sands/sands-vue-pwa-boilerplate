import { userService } from '../../_services';
import router from '../../router/index'
import {authmixin} from '../../mixins/auth.mixin'
import { globalmixin } from '@/mixins/global.mixin'

// let token = localStorage.getItem("token")

export const user = {
    namespaced: true,
    state: {
        users: [],
        usersOccupOperator: [],
        usersOccupCrewchief: [],
        current_page: 1,
        last_page: 1,
        total_rows: 0,
        per_page: 10,
        page_options: [5, 10, 20, 50, {
            value: Number.MAX_SAFE_INTEGER,
            text: "show all"
        }],
        filter: '',
        message: '',
        message_type: ''
    },
    getters: {
        getUsers: (state) => {
            let users = state.users
            users.map(function (v, k) {
                let dataBy = v.updated_by != "" ? v.updated_by : v.created_by
                let dataAt = v.updated_at != "" ? globalmixin.methods.formatDatetimeV1(v.updated_at) : globalmixin.methods.formatDatetimeV1(v.created_at)
                users[k]['data_by'] = dataBy
                users[k]['data_at'] = dataAt
                return v
            })
            // return state.users
            return users
        },
        getUsersOccupOperator: (state) => {
            var operators = []
            let usersOccupOperator = state.usersOccupOperator
            let usersOccupCrewchief = state.usersOccupCrewchief
            usersOccupOperator.forEach(el => {
                operators.push(el)
            });
            usersOccupCrewchief.forEach(el => {
                if(operators.some(item => item.id != el.id)){
                    operators.push(el)
                }
            });

            let sortKey = globalmixin.methods.sortedKeys(operators,"fullname",false,true)
            //build new Array by alphabet
            let newOperators = []
            sortKey.forEach(el => {
                newOperators.push(operators[el])
            });

            // return state.usersOccupOperator
            return newOperators
        },
        getUsersOccupCrewchief: (state) => {
            return state.usersOccupCrewchief
        },
        getPerPage: (state) => {
            return state.per_page
        },
        getTotalRows: (state) => {
            return state.total_rows
        },
    },
    actions: {
        fetchUsers({ dispatch, commit }, payload) {
            let token = localStorage.getItem("token")

            userService.getUsersTable(token, payload)
                .then(
                    data => {
                        commit('setUsers', data.data)
                    },
                    e => {
                        commit('clearUsers')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        fetchOccupOperator({ dispatch, commit }, payload) {
            let token = localStorage.getItem("token")

            userService.getUsersOccupOperator(token, payload)
                .then(
                    data => {
                        commit('setOccupOperator', data.data)
                    },
                    e => {
                        commit('clearUsers')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        fetchOccupChiefCrew({ dispatch, commit }, payload) {
            let token = localStorage.getItem("token")

            userService.getUsersOccupChiefCrew(token, payload)
                .then(
                    data => {
                        commit('setOccupChiefCrew', data.data)
                    },
                    e => {
                        commit('clearUsers')
                        let msg = authmixin.methods.getMessageAPIError(e)
                        commit('setMessage', { msg: msg, type: 'error' })
                    }
                )
        },
        create({dispatch, commit, state}, payload) {
            let token = localStorage.getItem("token")

            userService.createUser(token, payload)
            .then(
                data => {
                    let msg = data.msg

                    let payload = {
                        page: state.current_page,
                        search: "",
                        limit: state.per_page,
                    }
                    dispatch("fetchUsers", payload)
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        update({dispatch, commit, state}, payload) {
            let token = localStorage.getItem("token")

            userService.updateUser(token, payload)
            .then(
                data => {
                    let msg = data.msg

                    let payload = {
                        page: state.current_page,
                        search: "",
                        limit: state.per_page,
                    }
                    dispatch("fetchUsers", payload)
                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        },
        delete({dispatch, commit, state}, payload) {
            let token = localStorage.getItem("token")

            userService.deleteUser(token, payload)
            .then(
                data => {
                    let msg = data.msg
               
                    let payload = {
                        page: state.current_page,
                        search: "",
                        limit: state.per_page,
                    }
                    dispatch("fetchUsers", payload)

                    commit('setMessage', {msg: msg, type: 'success'})
                },
                e => {
                    let msg = authmixin.methods.getMessageAPIError(e)
                    commit('setMessage', { msg: msg, type: 'error' })
                }
            )
        }
    },
    mutations: {
        setUsers(state, data) {
            state.users = data.data
            state.current_page = data.current_page
            state.last_page = data.last_page
            state.total_rows = data.total
        },
        clearUsers(state) {
            state.users = []
            state.usersOccupCrewchief = []
            state.usersOccupOperator = []
        },
        setOccupOperator(state, data){
            state.usersOccupOperator = data
        },
        setOccupChiefCrew(state, data){
            state.usersOccupCrewchief = data
        },
        setMessage(state, msgOpt) {
            state.message = msgOpt.msg
            state.message_type = msgOpt.type
        },
        clearMessage(state) {
            state.message = ""
            state.message_type = ""
        },
        changeLimitPage(state, data){
            state.per_page = data
        },
        changeFilter(state, data){
            state.filter = data
        }
    }
}