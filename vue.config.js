module.exports = {
    baseUrl: "/",
    publicPath: "/",
    lintOnSave: false,

    pwa:{
        workboxPluginMode: "InjectManifest",
        workboxOptions:{
            swSrc:"./src/service-worker.js",
        }
    }
};
